Rails.application.routes.draw do
    get '/main', to: 'top#main'
    get '/login', to: 'top#login'
    post '/login', to: 'top#create'
    delete '/logout', to: 'top#destroy'
    root 'users#index'
    get 'top/main', to:'top#main'
    resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
